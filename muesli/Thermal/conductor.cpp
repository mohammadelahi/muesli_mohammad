/****************************************************************************
*
*                                 M U E S L I   v 1.8
*
*
*     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include "conductor.h"

// These are the ranges of temperature for changing the material properties in AMconductorMP
#define TRANSITION_SIZE_1 100.0
#define TRANSITION_SIZE_2 10.0

using namespace std;
using namespace muesli;



conductorMaterial::conductorMaterial(const std::string& name)
:
material(name)
{

}




conductorMaterial::conductorMaterial(const std::string& name,
                                       const materialProperties& cl)
:
material(name, cl)
{

}




bool conductorMaterial::check() const
{
    bool ret = true;
    return ret;
}




void conductorMaterial::setRandom()
{
    material::setRandom();
}




conductorMP::conductorMP(const conductorMaterial& mat) :
theConductor(&mat),
time_n(0.0),
time_c(0.0)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void conductorMP::commitCurrentState()
{
    time_n  = time_c;
    temp_n  = temp_c;
    gradT_n = gradT_c;
}




void conductorMP::resetCurrentState()
{
    time_c  = time_n;
    temp_c  = temp_n;
    gradT_c = gradT_n;
}




void conductorMP::setRandom()
{
    gradT_n.setRandom();
    gradT_c = gradT_n;
}




bool conductorMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    double tn1 = muesli::randomUniform(0.1,1.0);

    ivector gradT; gradT.setRandom();
    double temp = muesli::randomUniform(280.0, 1000.0);
    const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);

    // programmed heatflux and conductivity
    ivector q;  heatflux(q);
    istensor K; conductivity(K);
    ivector qp; heatfluxDerivative(qp);

    // compare heat flux with (minus) derivative of energy
    if (true)
    {
        // numerical differentiation heatflux
        ivector numFlux;
        numFlux.setZero();
        const double inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            double original = gradT(i);

            gradT(i) = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wp1 = thermalEnergy();

            gradT(i) = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wp2 = thermalEnergy();

            gradT(i) = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wm1 = thermalEnergy();

            gradT(i) = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wm2 = thermalEnergy();


            // fourth order approximation of the derivative
            double der = - (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);
            numFlux(i) = der;

            gradT(i) = original;
        }

        // relative error less than 0.01%
        ivector error = numFlux - q;
        isok = (error.norm()/q.norm() < 1e-4);

        os << "\n   1. Comparing heat flux with derivative of thermal energy.";

        if (!isok)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Heat flux:\n" << q;
            os << "\n   Numeric heat flux:\n" << numFlux;
        }
        else
            os << " Test passed.";
    }
    

    // compare conductivity tensor with (minus) derivative of heat flux wrt temp gradient
    if (true)
    {
        istensor numK;
        numK.setZero();
        const double inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            double original = gradT(i);

            gradT(i) = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp1; heatflux(Qp1);

            gradT(i) = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp2; heatflux(Qp2);

            gradT(i) = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm1; heatflux(Qm1);

            gradT(i) = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm2; heatflux(Qm2);

            // fourth order approximation of the derivative
            ivector der = - (-Qp2 + 8.0*Qp1 - 8.0*Qm1 + Qm2)/(12.0*inc);
            numK(0,i) = der(0);
            numK(1,i) = der(1);
            numK(2,i) = der(2);

            gradT(i) = original;
        }

        // relative error less than 0.01%
        istensor error = numK - K;
        isok = (error.norm()/q.norm() < 1e-4);

        os << "\n   2. Comparing conductivity tensor with derivative of heat flux.";

        if (!isok)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Conductivity flux:\n" << K;
            os << "\n   Numeric conductivity :\n" << numK;
        }
        else
            os << " Test passed.";
    }

    // compare qprime with derivative of heat flux wrt temp
    if (true)
    {
        ivector numQ;
        numQ.setZero();
        const double inc = 1.0e-3;

        {
            double original = temp;

            temp = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp1; heatflux(Qp1);

            temp = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp2; heatflux(Qp2);

            temp = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm1; heatflux(Qm1);

            temp = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm2; heatflux(Qm2);

            // fourth order approximation of the derivative
            ivector der = (-Qp2 + 8.0*Qp1 - 8.0*Qm1 + Qm2)/(12.0*inc);
            numQ = der;

            temp = original;
        }

        // relative error less than 0.01%
        ivector error = numQ - qp;
        isok = (error.norm()/qp.norm() < 1e-4);

        os << "\n   3. Comparing qprime with derivative of heat flux.";

        if (!isok && qp.norm() > 1e-8)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Q prime: " << qp;
            os << "\n   Numeric derivative : " << numQ;
        }
        else
            os << " Test passed.";
    }


    return isok;
}




void conductorMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
    time_c  = theTime;
    temp_c  = temp;
    gradT_c = gradT;
}




fourierMaterial::fourierMaterial(const std::string& name,
                                   const materialProperties& cl)
:
  conductorMaterial(name, cl),
  _conductivity(0.0),
  _capacity(0.0)
{
    muesli::assignValue(cl, "conductivity", _conductivity);
    muesli::assignValue(cl, "capacity", _capacity);
}




fourierMaterial::fourierMaterial(const std::string& name, const double k, const double rho)
:
conductorMaterial(name),
_conductivity(k)
{
}




fourierMaterial::fourierMaterial(const std::string& name) :
conductorMaterial(name),
_conductivity(0.0)
{
}




bool fourierMaterial::check() const
{
    bool ret = conductorMaterial::check();

    if (_conductivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in fourier material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* fourierMaterial::createMaterialPoint() const
{
    fourierMP *mp = new fourierMP(*this);
    return mp;
}




double fourierMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if      (p == PR_CONDUCTIVITY)        ret = _conductivity;
    else if (p == PR_THERMAL_CAP)         ret = _capacity;
    else
    {
        std::cout << "\n Error in GetfourierMaterialProperty.";
    }
    return ret;
}




void fourierMaterial::print(std::ostream &of) const
{
    of  << "\n   Isotropic thermally conducting material "
        << "\n   Conductivity      : " << _conductivity
        << "\n   Density           : " << density()
        << "\n   Ref. Heat capacity: " << _capacity;
}




void fourierMaterial::setRandom()
{
    conductorMaterial::setRandom();
    _conductivity = muesli::randomUniform(1.0, 10.0);
    _capacity     = muesli::randomUniform(1.0, 10.0);
}




bool fourierMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;
    return isok;
}




fourierMP::fourierMP(const fourierMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void fourierMP::conductivity(istensor& K) const
{
    K = istensor::identity() * mat->_conductivity;
}




void fourierMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = mat->_conductivity * na.dot(nb);
}




double fourierMP::density() const
{
    return theConductor->density();
}




materialState fourierMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState fourierMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double fourierMP::heatCapacity() const
{
    return mat->_capacity*theConductor->density();
}




double fourierMP::heatCapacityDerivative() const
{
    return 0.0;
}




void fourierMP::heatflux(ivector &q) const
{
    q = (- mat->_conductivity) * gradT_c;
}




void fourierMP::heatfluxDerivative(ivector &qprime) const
{
    qprime.setZero();
}




void fourierMP::setRandom()
{
    conductorMP::setRandom();
}




double fourierMP::thermalEnergy() const
{
     return 0.5*mat->_conductivity * gradT_c.dot(gradT_c);
}




controlledfourierMaterial::controlledfourierMaterial(const std::string& name,
                                   const materialProperties& cl)
:
  conductorMaterial(name, cl),
  _initialconductivity(0.0),
  _capacity(0.0)
{
    muesli::assignValue(cl, "conductivity", _initialconductivity);
    muesli::assignValue(cl, "capacity", _capacity);
}




controlledfourierMaterial::controlledfourierMaterial(const std::string& name, const double k, const double rho)
:
conductorMaterial(name),
_initialconductivity(k)
{
}




controlledfourierMaterial::controlledfourierMaterial(const std::string& name) :
conductorMaterial(name),
_initialconductivity(0.0)
{
}




bool controlledfourierMaterial::check() const
{
    bool ret = conductorMaterial::check();

    if (_initialconductivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in controlled fourier material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* controlledfourierMaterial::createMaterialPoint() const
{
    controlledfourierMP *mp = new controlledfourierMP(*this);
    return mp;
}




double controlledfourierMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if      (p == PR_CONDUCTIVITY)        ret = _initialconductivity;
    else if (p == PR_THERMAL_CAP)         ret = _capacity;
    else
    {
        std::cout << "\n Error in GetfourierMaterialProperty.";
    }
    return ret;
}




void controlledfourierMaterial::print(std::ostream &of) const
{
    of  << "\n   Controlled Isotropic thermally conducting material "
        << "\n   Initial Conductivity      : " << _initialconductivity
        << "\n   Density                   : " << density()
        << "\n   Ref. Heat capacity        : " << _capacity;
}




void controlledfourierMaterial::setRandom()
{
    conductorMaterial::setRandom();
    _initialconductivity = muesli::randomUniform(1.0, 10.0);
    _capacity            = muesli::randomUniform(1.0, 10.0);
}




bool controlledfourierMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;
    return isok;
}




controlledfourierMP::controlledfourierMP(const controlledfourierMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
    _conductivity = mat->_initialconductivity;
}




void controlledfourierMP::conductivity(istensor& K) const
{
    K = istensor::identity() * _conductivity;
}




void controlledfourierMP::setconductivity(double kappa)
{
    _conductivity = kappa;
}




void controlledfourierMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = _conductivity * na.dot(nb);
}




double controlledfourierMP::density() const
{
    return theConductor->density();
}




materialState controlledfourierMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState controlledfourierMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double controlledfourierMP::heatCapacity() const
{
    return mat->_capacity*theConductor->density();
}




double controlledfourierMP::heatCapacityDerivative() const
{
    return 0.0;
}




void controlledfourierMP::heatflux(ivector &q) const
{
    q = (- _conductivity) * gradT_c;
}




void controlledfourierMP::heatfluxDerivative(ivector &qprime) const
{
    qprime.setZero();
}




void controlledfourierMP::setRandom()
{
    conductorMP::setRandom();
}




double controlledfourierMP::thermalEnergy() const
{
     return 0.5*_conductivity * gradT_c.dot(gradT_c);
}




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name,
                                                           const materialProperties& cl)
:
conductorMaterial(name, cl),
_capacity(0.0)
{
    itensor t;
    muesli::assignValue(cl, "kxx", t(0,0));
    muesli::assignValue(cl, "kxy", t(0,1));
    muesli::assignValue(cl, "kxz", t(0,2));
    muesli::assignValue(cl, "kyx", t(1,0));
    muesli::assignValue(cl, "kyy", t(1,1));
    muesli::assignValue(cl, "kyz", t(1,2));
    muesli::assignValue(cl, "kzx", t(2,0));
    muesli::assignValue(cl, "kzy", t(2,1));
    muesli::assignValue(cl, "kzz", t(2,2));

    _K = istensor::symmetricPartOf(t);
    muesli::assignValue(cl, "capacity", _capacity);
}




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name,
                                                           double kxx, double kxy, double kxz,
                                                           double kyy, double kyz, double kzz,
                                                           double rho, double capacity)
:
conductorMaterial(name),
_capacity(capacity)
{
    _K(0,0) = kxx;
    _K(0,1) = _K(1,0) = kxy;
    _K(0,2) = _K(2,0) = kxz;
    _K(1,1) = kyy;
    _K(1,2) = _K(2,1) = kyz;
    _K(2,2) = kzz;
 }




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name) :
conductorMaterial(name),
_capacity(0.0)
{
    _K.setZero();
}




bool anisotropicConductorMaterial::check() const
{
    bool ret = conductorMaterial::check();

    ivector ev = _K.eigenvalues();
    if ( ev.min() <= 0.0)
    {
        ret = false;
        std::cout << "Error in anisotropicConductor material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* anisotropicConductorMaterial::createMaterialPoint() const
{
    anisotropicConductorMP *mp = new anisotropicConductorMP(*this);
    return mp;
}




double anisotropicConductorMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    return ret;
}




void anisotropicConductorMaterial::print(std::ostream &of) const
{
    of  << "\n   Anisotropic thermally conducting material "
        << "\n   Conductivity kxx   : " << _K(0,0)
        << "\n   Conductivity kxy   : " << _K(0,1)
        << "\n   Conductivity kxz   : " << _K(0,2)
        << "\n   Conductivity kyy   : " << _K(1,1)
        << "\n   Conductivity kyz   : " << _K(1,2)
        << "\n   Conductivity kzz   : " << _K(2,2)
        << "\n   Density            : " << density()
        << "\n   Ref. Heat capacity : " << _capacity;
}




void anisotropicConductorMaterial::setRandom()
{
    conductorMaterial::setRandom();
    _capacity = muesli::randomUniform(1.0, 10.0);

    _K.setRandom();
}




bool anisotropicConductorMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;

    return isok;
}




anisotropicConductorMP::anisotropicConductorMP(const anisotropicConductorMaterial& theMaterial)
:
conductorMP(theMaterial),
mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void anisotropicConductorMP::conductivity(istensor& K) const
{
    K = mat->_K;
}




void anisotropicConductorMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = na.dot(mat->_K * nb);
}




double anisotropicConductorMP::density() const
{
    return theConductor->density();
}




materialState anisotropicConductorMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState anisotropicConductorMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




double anisotropicConductorMP::heatCapacity() const
{
    return mat->_capacity*theConductor->density();
}




double anisotropicConductorMP::heatCapacityDerivative() const
{
    return 0.0;
}




void anisotropicConductorMP::heatflux(ivector &q) const
{
    q = - (mat->_K * gradT_c);
}




void anisotropicConductorMP::heatfluxDerivative(ivector &qprime) const
{
    qprime.setZero();
}




void anisotropicConductorMP::setRandom()
{
    conductorMP::setRandom();
}




double anisotropicConductorMP::thermalEnergy() const
{
    return 0.5* gradT_c.dot(mat->_K * gradT_c);
}
